(function( $ ) {
	var methods = {
		init: function (context) {
			$.fn.tooltip("hide");
			
			$(".tooltip").unbind("mouseover mousemove mouseout");
			$(".tooltip").bind("mouseover mousemove mouseout", function (event) {
				$.fn.tooltip("handleEvent", event);
			});
		},
		handleEvent: function (event) {
			event = $.event.fix(event);
			if (event.preventDefault) event.preventDefault();
			
			var elm = event.target;
			while (!$(elm).hasClass("tooltip"))
			{
				elm = $(elm).parent();
			}
			
			var tmp_top = position.getPositionY(event);
			var tmp_left = position.getPositionX(event);
			
			if (event.type == "mouseout")
			{
				$.fn.tooltip("hide");
			}
			else if (event.type == "mouseover")
			{
				$("body").append(
					$("<div>").attr("id", "tooltip")
					.html($(elm).attr("title"))
					.css({
						position: "absolute",
						top: tmp_top+"px",
						left: tmp_left+"px",
						margin: 0,
						"z-index": 110
					})
				);
				
				if (tmp_top + 12 + $("#tooltip").outerHeight(true) > $(window).height() + $(window).scrollTop())
				{
					tmp_top = $(window).height() + $(window).scrollTop() - $("#tooltip").outerHeight(true) - 12;
				}
				
				if (tmp_left + 12 + $("#tooltip").outerWidth(true) > $(window).width()+$(window).scrollLeft())
				{
					tmp_left = $(window).width() + $(window).scrollLeft() - $("#tooltip").outerWidth(true) - 12;
				}
				
				$("#tooltip").css({
					top: (tmp_top+12)+"px",
					left: (tmp_left+12)+"px"
				});

				$(elm).attr("title", "");
				
				$("#tooltip").data("triggerElement", elm);
			}
			else if (event.type == "mousemove")
			{				
				if (tmp_top + 12 + $("#tooltip").outerHeight(true) > $(window).height() + $(window).scrollTop())
				{
					tmp_top = $(window).height() + $(window).scrollTop() - $("#tooltip").outerHeight(true) - 12;
				}
				
				if (tmp_left + 12 + $("#tooltip").outerWidth(true) > $(window).width()+$(window).scrollLeft())
				{
					tmp_left = $(window).width() + $(window).scrollLeft() - $("#tooltip").outerWidth(true) - 12;
				}
				
				$("#tooltip").css({
					top: (tmp_top+12)+"px",
					left: (tmp_left+12)+"px"
				});
			}
		},
		hide: function () {
			$($("#tooltip").data("triggerElement")).attr("title", $("#tooltip").html());
			
			$("#tooltip").remove();
			$("#tooltip").removeData("triggerElement");
		}
	};
	$.fn.tooltip = function (method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else if (typeof method === "object" || !method) {
			return methods.init.apply(this, arguments);
		}
		else {
			$.error("Method "+method+" does not exist on jQuery.overlay");
		}
	};
})( jQuery );

$(document).ready(function () {
	$.fn.tooltip("init");
});