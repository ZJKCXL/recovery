<?php
class Globals {
  public static $META_DESC_LENGTH_IDEAL = 160; 
  public static $META_KEYWORDS_LENGTH_IDEAL = 160; 
  public static $META_TITLE_LENGTH_IDEAL = 60; 
  public static $META_DESC_LENGTH = 200; 
  public static $META_KEYWORDS_LENGTH = 200; 
  public static $META_TITLE_LENGTH = 70;  
  public static $GLOBAL_ADMINPAGES = 'adminpages';
  public static $GLOBAL_DASHBOARD = '1vv-MwKQBJqDpCY0GVluPc2bFKNax4kVh';
  public static $GLOBAL_FILEMANAGER = 1;   //0 neni, 1 je
  public static $GLOBAL_HOME = "/htmlfiles/WWW";   //puvodni globalgal
  public static $GLOBAL_TEMP_DIR = '';   
  public static $GLOBAL_BASE_URL = 'http://www.sananim.cz/';
  public static $GLOBAL_BASE_URL_FB = 'http://www.sananim.cz/Uploads/fb/';      
  public static $GLOBAL_BASE_URL_IMGS = 'http://www.sananim.cz/Uploads/web_images/';  
  public static $GLOBAL_WEB_IMGS_PATH = '../Uploads/web_images/';   
  public static $GLOBAL_DOWN =  "../Uploads/downloads/";
  public static $GLOBAL_DOWN_PREV =  "../Uploads/downloads/preview_images/";
  public static $GLOBAL_DOWN_PREV_SMALL =   "../Uploads/downloads/preview_images/thumbs/";
  public static $GLOBAL_DOWN_SHOW =  "http://www.sananim.cz/Uploads/downloads/";
  public static $GLOBAL_DOWN_PREV_SHOW =  "http://www.sananim.cz/Uploads/downloads/preview_images/";
  public static $GLOBAL_DOWN_PREV_SMALL_SHOW =   "http://www.sananim.cz/Uploads/downloads/preview_images/thumbs/";  
  public static $GLOBAL_GAL_IMGS_URL = '/finalgalpics/';   
  public static $GLOBAL_WEB_IMGS_URL = '/Uploads/web_images/';      
  public static $GLOBAL_WEB_IMGS_OPENER = "swipebox";
  public static $GLOBAL_MODULES_BLOG = 'Zařízení';   
  public static $GLOBAL_MODULES_MEMBERS = 'Lidé';     
  public static $GLOBAL_ADMIN_PAGER = 15; 
  public static $GLOBAL_GAL_REVERSE = 15; 
  public static $GLOBAL_SQL_FILE = '/sql/db.php'; 
  public static $GLOBAL_HEADERS_STICKY = 1;   
  public static $GLOBAL_HEADERS_FONTAWESOME = 1;     
  public static $GLOBAL_HEADERS_JQUERY = 1;   
  public static $GLOBAL_HEADERS_sWIPEBOX = 1;   
  public static $GLOBAL_HEADERS_SLICK = 1; 
  public static $GLOBAL_HEADERS_RESPCSS = 1;  
  public static $GLOBAL_HEADERS_TINYCSS = 1;  
  public static $GLOBAL_WEB_NAME = "Sananim z.ú.";
  // pouziti pak Globals::$GLOBAL_WEB_IMGS_OPENER
 }
// echo Globals::$GLOBAL_DOWN_SHOW;
?>
