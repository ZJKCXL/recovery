<?
include ("logger.php");

define("NO_ERR",0);

define("CONN_CANNOT_CONNECT_RDBMS",-1);
define("CONN_CANNOT_SET_CHARSET",-2);
define("CONN_CANNOT_SELECT_DB",-3);

define("DATA_EMPTY_QUERY",-10);
define("DATA_QUERY_FAILED",-11);
define("DATA_NO_DATA",-12);
define("DATA_NO_FURTHER_DATA",-13);

define("QUERY_IS_SELECT",-21);
define("QUERY_IS_INSERT",-22);
define("QUERY_IS_UPDATE",-23);
define("QUERY_IS_DELETE",-24);
define("QUERY_IS_UKNOWN",-25);

define("OBJECT_NO_GID", -100);
define("OBJECT_CANNOT_LOAD", -101);
define("OBJECT_CANNOT_CREATE", -102);

define("OBJECTLIST_OUT_OF_BOUNDS",-110);
define("OBJECTLIST_CANNOT_EXTEND",-111);
define("OBJECTLIST_CANNOT_DELETE",-112);
define("OBJECTLIST_GID_NOT_FOUND",-113);

define("LOGGER_UNKNOWN_LOG_TYPE", -200);
define("LOGGER_EMPTY_MESSAGE", -201);
define("LOGGER_CANNOT_OPEN_FILE", -202);

define("VALIDATE_STRING_UNKNOWN_TYPE", -300);
define("VALIDATE_STRING_EMPTY", -301);
define("VALIDATE_STRING_INVALID", -302);

define("GID_INVALID_PARAMS", -400);
define("GID_NO_GID", -401);
define("GID_INVALID_GID", -402);

define("ENUM_NO_SUCH_RECORD", -120);
define("ENUM_INVALID_ID", -121);
define("ENUM_INVALID_DESC", -122);
define("ENUM_CANNOT_UPDATE_DESC", -123);
define("ENUM_CANNOT_DELETE", -124);
define("ENUM_CANNOT_ADD_NEW", -125);
define("ENUM_EMPTY_SET", -126);

define("RELATION_EMPTY_SET",-130);
define("RELATION_CANNOT_DELETE",-131);
define("RELATION_INVALID_DATA",-132);
define("RELATION_INVALID_ID",-133);
define("RELATION_CANNOT_ADD_NEW",-134);

define("HISTORY_VALUE_EMPTY_SET",-140);
define("HISTORY_VALUE_CANNOT_DELETE",-141);
define("HISTORY_VALUE_INVALID_DATA",-142);
//define("RELATION_INVALID_ID",-133);
define("HISTORY_VALUE_CANNOT_ADD_NEW",-144);

define("OBJECT_SNAPSHOTER_INVALID_ID",-150);
define("OBJECT_SNAPSHOTER_COULD_NOT_RETRIEVE_DATA",-151);
define("OBJECT_SNAPSHOTER_UNSERIALIZE_FAILED",-152);
define("OBJECT_SNAPSHOTER_INVALID_INPUT_OBJECT",-153);
define("OBJECT_SNAPSHOTER_MAKE_SNAPSHOT_FAILED",-154);
define("OBJECT_SNAPSHOTER_INVALID_GID",-155);

define("SNAPSHOTED_OBJECT_COULD_NOT_SAVE",-160);

define("SECURITY_INVALID_IDENTITY_GID", -501);
define("SECURITY_INVALID_OBJECT_GID", -502);
define("SECURITY_INVALID_RIGHT", -503);
define("SECURITY_CANNOT_GRANT", -504);
define("SECURITY_CANNOT_DENY", -505);
define("SECURITY_CANNOT_REVOKE", -506);
define("SECURITY_CANNOT_GET_RIGHT", -507);

define("USER_INVALID_USER_GID", -521);
define("USER_INVALID_GROUP_GID", -522);
define("USER_CANNOT_GET_MEMBERSHIP",-523);

define("GROUP_INVALID_GROUP_GID", -531);
define("GROUP_INVALID_MEMBER_GID", -532);
define("GROUP_CANNOT_GET_MEMBERS",-533);
define("GROUP_CANNOT_ADD_MEMBER",-534);
define("GROUP_CANNOT_GET_MEMBERSHIP",-535);
define("GROUP_MEMBER_ALREADY_EFFECTIVE_MEMBER_OF",-536);
define("GROUP_GROUP_IS_EFFECTIVE_MEMBER_OF",-537);
define("GROUP_SAME_EFFECTIVE_PARENT",-538);
define("GROUP_CANNOT_DELETE_MEMBER",-539);
define("GROUP_MEMBER_NOT_MEMBER_OF", -540);

define("OWNER_INVALID_OBJECT_GID", -550);
define("OWNER_INVALID_OWNER_GID", -551);
define("OWNER_CANNOT_DELETE_OWNERSHIP",-552);
define("OWNER_CANNOT_ADD_OWNERSHIP",-553);
define("OWNER_NO_OWNERSHIP_INFO",-554);
define("OWNER_NO_OWNED_OBJECTS",-555);

define("LOGIN_INVALID_LOGIN_PASS", -1000);
define("LOGIN_FAILED", -1001);
define("LOGIN_INVALID_ACCESS_TIME", -1002);
define("LOGIN_ACCOUNT_DISABLED", -1003);
define("LOGIN_ACCOUNT_DELETED", -1004);
define("LOGOUT_NOT_LOGGED_IN", -1005);
define("LOGOUT_ERROR", -1006);
define("LOGIN_ERROR", -1007);
define("LOGOUT_FAILED", -1008);
define("LOGGED_IN_INVALID_PARAM", -1009);
define("USER_LEVEL_INVALID_PARAM", -1010);
define("NOT_LOGGED_IN", -1011);

class Error
{
	private $errcode;

	private static $do_not_log_errcode = array(-12, -13, -301);

	public function __construct($inErrCode, $ext_info=null)
	{
		if (isset($inErrCode) && is_numeric($inErrCode))
		{
			$this->errcode = $inErrCode;
			if ($inErrCode != NO_ERR)
			{
				self::logIt($inErrCode,$ext_info);
			}
		}
		else
		{
			return false;
		}
	}

	protected static function logIt($inErrCode, $ext_info=null)
	{
		$msg_string = "Error ".$inErrCode." met";
		if (isset($ext_info) && strlen($ext_info)>0)
		{
			$msg_string.=", extended info : >> ".$ext_info." <<";
		}

		if (array_search($inErrCode, self::$do_not_log_errcode) === false)
		{
			Logger::logToFile($msg_string, "error");
		}
	}

	public function errCode()
	{
		if (isset($this->errcode) && is_numeric($this->errcode))
		{
			return $this->errcode;
		}
		else
		{
			return false;
		}
	}
}

function error_get_by_id ($const)
{
	$query = "SELECT tblError.*";
	$query .= " FROM (tblError)";
	$query .= " WHERE 1";
	$query .= " AND tblError.errConst='".$const."'";

	$db = new Data();
	$db->doQuery($query);

	$result = parse_mysql_result($db);
	if (@count($result) == 1)
	{
		$result = $result[0];
	}

	return $result;
}

function error_cs_get_by_id ($const)
{
	$query = "SELECT tblErrorCS.*";
	$query .= " FROM (tblErrorCS)";
	$query .= " WHERE 1";
	$query .= " AND tblErrorCS.id=".$const;

	$db = new Data();
	$db->doQuery($query);

	$result = parse_mysql_result($db);
	if (@count($result) == 1)
	{
		$result = $result[0];
	}

	return $result;
}

?>