<?

function get_lang ()
{
	$lang = (strlen($_COOKIE["lang"]) == 2) ? $_COOKIE["lang"] : LANG_DEFAULT;
	$lang = (strlen($_POST["lang"]) == 2) ? $_POST["lang"] : $lang;
	$lang = (strlen($_GET["lang"]) == 2) ? $_GET["lang"] : $lang;
	return $lang;
}

function get_geoloc_lang ()
{
	$location_lang = @file_get_contents("http://api.hostip.info/country.php?ip=".$_SERVER["REMOTE_ADDR"]);
	$location_lang = $location_lang != "EN" ? "cz" : "en";
	
	return $location_lang;
}

function get_page ($lang = LANG_DEFAULT, $page_id = null)
{
	$result = false;
	$currlang = get_lang() == "cz" ? "" : get_lang()."_";

	if (is_null($page_id))
	{
		$link = $_REQUEST["page"];
		$link = $_REQUEST["section"] == PAGE_LINK_GALLERY ? $_REQUEST["section"] : $link;
		$link = $_REQUEST["section"] == "blog" ? $_REQUEST["section"] : $link;
		$id_query = "SELECT ID";
		$id_query .= " FROM (".$currlang."tblpages)";
		$id_query .= " WHERE 1";
		$id_query .= (strlen($link) <= 0) ? " AND pagesHomepage=1" : " AND pagesLink='".$link."'";
		$db = new Data();
		$db->doQuery($id_query);
		if ($db->getNumRows() != 1)
		{
			return $result;
		}
		$id = $db->getFetchedDataLine();
		$id = $id["ID"];
	}
	else
	{
		$id = $page_id;
	}

	$lang = $lang == "cz" ? "" : $lang."_";
	$ulang = strtoupper($lang);

	$query = "SELECT ".$lang."tblpages.*";
	$query .= ", ".$lang."menugroups.menuType";
	$query .= ", ".$lang."menugroups.menuSection";
	$query .= " FROM (".$lang."tblpages)";
	$query .= " LEFT JOIN ".$lang."menugroups ON (".$lang."menugroups.id=".$lang."tblpages.pagesMenu)";
	$query .= " WHERE 1";
	$query .= " AND ".$lang."tblpages.ID=".$id;
	
	//echo $query;

	$db = new Data();
	$db->doQuery($query);

	if ($db->getNumRows() == 1)
	{
		$page = $db->getFetchedDataLine($db);

		if ($_REQUEST["section"] == PAGE_LINK_GALLERY)
		{
			$result .= $page["pagesLink"]."/";
			$result .= $_REQUEST["note"]."/";

			$query_galery = "SELECT tblgalery.gal_name".$ulang;
			$query_galery .= " FROM (tblgalery)";
			$query_galery .= " WHERE 1";
			$query_galery .= " AND id=".$_REQUEST["note"];

			$db->doQuery($query_galery);
			if ($db->getNumRows() == 1)
			{
				$gal_name = $db->getFetchedDataLine();

				$result .= normalize_filename($gal_name["gal_name".$ulang]).".html";
			}
		}
		elseif ($_REQUEST["section"] == "blog")
		{
			$result = "blog.html";
		}
		else
		{
			if ($page["pagesHomepage"] != 1)
			{
				$result = $page["menuType"] == 0 ? $page["menuSection"]."/" : "";
				$result .= $page["pagesLink"].".html";
			}
		}

		$tmp_gets = $_GET;
		unset($tmp_gets["page"]);
		unset($tmp_gets["section"]);
		unset($tmp_gets["note"]);
		unset($tmp_gets["lang"]);

		$first = true;
		$query_string = "";
		if (is_array($tmp_gets))
		{
			foreach ($tmp_gets as $tmp_get_key => $tmp_get_value)
			{
				$query_string .= ($first) ? "?" : "&";
				$query_string .= $tmp_get_key."=".$tmp_get_value;

				$first = false;
			}
		}

		$result .= $query_string;
	}

	return $result;
}

function get_section ($lang = LANG_DEFAULT, $section_id = null)
{
	$result = false;

	$currlang = get_lang() == "cz" ? "" : get_lang();

	if (is_null($section_id))
	{
		$link = $_REQUEST["section"];

		$id_query = "SELECT ID";
		$id_query .= " FROM (".$currlang."menugroups)";
		$id_query .= " WHERE 1";
		$id_query .= " AND pagesLink='".$link."'";

		$db = new Data();
		$db->doQuery($id_query);

		if ($db->getNumRows() != 1)
		{
			return $result;
		}
		$id = $db->getFetchedDataLine();
		$id = $id["ID"];
	}
	else
	{
		$id = $section_id;
	}

	$lang = $lang == "cz" ? "" : $lang;
	$ulang = strtoupper($lang);

	$query = "SELECT ".$lang."menugroups.menuSection";
	$query .= " FROM (".$lang."menugroups)";
	$query .= " WHERE 1";
	$query .= " AND ".$lang."menugroups.ID=".$id;

	$db = new Data();
	$db->doQuery($query);

	if ($db->getNumRows() == 1)
	{
		$section = $db->getFetchedDataLine($db);

		$result = $section["menuSection"];
	}

	return $result;
}

function get_pp_kind ($domain = "")
{
	if ($domain == "hbox")
	{
		switch (get_lang())
		{
			default:
			case "cz":
				$result = 4;
				break;
			case "ru":
				$result = 8;
				break;
			case "en":
				$result = 5;
				break;
		}
	}
	else
	{
		switch (get_lang())
		{
			default:
			case "cz":
				$result = 0;
				break;
			case "ru":
				$result = 7;
				break;
			case "en":
				$result = 3;
				break;
		}
	}
	
	return $result;
}

?>
