<fieldset>   <style>
.allIcons .fa:before {
 width: 54px;
 display: block;
 text-align: center;
}
.allIcons .fa:before {
  content: '';
   line-height: 54px;
   font-size: 25px;
   font-weight: 400;
   text-shadow: 0px 0px 8px #000000;
   text-align: center;
   display: block;
   width: 60px;
   height: 60px;
   background-image: url('/picto_svg/case.svg')
}
.allIcons input[type="checkbox"] {
 display: none;
}

.allIcons input#all {
 display: block;
}

.allIcons input[type="checkbox"] + label {
 background: #d4d4d4;
}

.allIcons label {
 position: relative;
 margin-left: 3px;
 color: #17a2b8;
 cursor: pointer;
 margin-bottom: 35px;
}
 

.allIcons input[type="checkbox"]:checked + label:after {
 position: absolute;
 bottom: -5px;
 right: -5px;
 width: 25px;
 height: 25px;
 content: '\f00c';
 font-size: 25px;
 text-shadow: 1px 2px 3px #000;
}

.allIcons input[type="checkbox"]:checked + label.all:after {
 display: none;
 content: ' ';

}
.allIcons label span {
   font-family: sans-serif;
   font-size: 8px;
   color: #000;
   display: block;
   text-align: center;
   height: 2em;
   text-transform: uppercase;
   padding-top: 5px;
   position: absolute;
   bottom: -20px;
   left: 2px;
   width: 50px;
    
}
</style>
  <div class="allIcons">
  <?php
 

  $pictoquery = "SELECT * FROM tbl_app_iconz Where icoPublic = 1  Order by icoOrder ";
  $pictores = $link->query($pictoquery);
  if ($pictores && @mysqli_num_rows($pictores)>0)   
  { 
  while ($picturow  = mysqli_fetch_array($pictores)  ){
    $showico =  $picturow['icoFile'];
    $titleIco =  $picturow['icoName'];
    $showID =  $picturow['ID'];
    $icount++;

    $icofind = "SELECT * FROM `tbl_app_search_content` WHERE `contentType` = ".$kind." AND `contentID` = '".$newsid."'  AND contentIco = '".$showID."' ";
    $icofindres = $link->query($icofind);
    if ($icofindres &&  @mysqli_num_rows($icofindres)>0)   
    {  
        $checked = ' checked ';
    }
    else{
      $checked = '';
    }
    ?>
     <input id="<?php echo $showico; ?>" class="checkbox" name="searchTypes[<?php echo $icount; ?>]" type="checkbox" value="<?php echo $showID; ?>"  <?php echo $checked; ?> />
     <label for="<?php echo $showico; ?>" class="<?php echo $showico; ?> fa" title="<?php echo $titleIco; ?>"><span><?php echo $titleIco; ?></span></label>
     <style>
     .allIcons .<?php echo $showico; ?>:before {
      background-image: url('/picto_svg/<?php echo $showico; ?>.svg')
      }
     </style>
    <?php
  }
  }
  ?>

  </div>
  <p>&nbsp;</p>
  <input type="submit" class="btn btn-lg btn-primary " value="Uložit" name="send" /> 
  <input type="submit" value="Uložit a pokračovat" name="send" class="btn btn-lg btn-info">  
 
  </fieldset>