<?php $number = $_REQUEST['number']; ?>
<fieldset class="col-lg-12  col-sm-12" id="otazka<?php echo $number; ?>"> 
<legend>Otázka č. <?php echo $number; ?></legend>

<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="inputGroup-sizing-default">
        Text otázky:
    </span>
    </div>
<input value="<? echo @$row["questName"]; ?>" name="questName[<?php echo $number; ?>]" type="text"  class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
</div>
<input type="hidden" name="questionOrder[<?php echo $number; ?>]" value="<?php echo $number; ?>">

 

<div class="input-group mb-3">
  <div class="input-group-prepend">
  <label class="input-group-text" for="inputGroupSelect01">Typ otázky:</label>
  </div>
  <div class="form-control one-input">
  <select name="questionTyp[<?php echo $number; ?>]" id="sel<?php echo $number; ?>" onChange="checkSelect(sel<?php echo $number; ?>)">
    <option value="1">Odpovědět textem</option>
    <option value="2">Označení odpovědi na škále</option>
    <option value="3">Zaškrtávání možností - pouze jednu</option>
    <option value="4">Zaškrtávání možností - více možností</option>
 </select>
</div>
</div>
 



<div id="semka<?php echo $number; ?>"></div>
 
<a class="far fa-question-circle nextqest" id="nextq<?php echo $number; ?>" onClick="nextQuest(<?php echo $number; ?>);return false;" title="Další otázka"><span>Přidat otázku </span></a> 

</fieldset>
<div id="tu<?php echo $number; ?>" class='qceep col-lg-12  col-sm-12 ask'></div>