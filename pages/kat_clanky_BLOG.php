<script>
$('.collapse').collapse();
 
</script>
<style>
#firstBox {
    border: 1px solid  #3AAFA9;
    padding: 1.5em 1.5em 0 1.5em  ;
    margin: 0 ;
    height: 270px;
    overflow: auto;
    background: #fff;
    display: flex;
    flex-direction: row;
}
#firstBox  h3:first-of-type {
    margin-top: 0;
}
#firstBox p + h3 {
    margin-top: 1 em!important;
}
.card {
    position: relative;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid #3AAFA9;
    border-top: 0px;
    

}
.card:first-of-type {
    border-top: 1px solid #3AAFA9;
}
.card-header {
    padding: 0;
    margin-bottom: 0;
    background-color: #fff;
}
 
.card-body {
    padding: .75rem 1.25rem;
}
.btn:not(:disabled):not(.disabled) {
    cursor: pointer;
}
.btn-link {
    font-weight: 400;
    color: #007bff;
    background-color: transparent;
}
.collapse {
    display: none;


}
.collapse.show {
    display: block;
}
#accordion .card-header h5 {
    margin: 0;
    padding: 0;
    display: block;
}

#accordion .card-header h5 button {
    padding: .75rem 1.25rem;
    border: none;
    background: #3AAFA9;
    color: #fff;
    width: 100%;
    text-align: left;
    font-size: 15px;
    font-family: 'PT Sans', sans-serif;
    line-height: 1.1em;
    font-weight: bold;
    text-transform: uppercase;
    border: none!important;
 
}
#accordion .card-header h5 button.collapsed {
    background: #fff;   
    color: #1D1D1B;
}
#content-right h3 {
    color: #1D1D1B;
    text-align: left;
    font-size: 15px;
    font-family: 'PT Sans', sans-serif;
    line-height: 1.1em;
    font-weight: bold;
    text-transform: uppercase;
    border: none!important;
}
button:focus {outline:0;}
#content-right .pparts {
   border: 1px solid #3AAFA9;
   padding: 1em;
   background: #fff;
   
}
#content-right .pparts:first-of-type {
    background: #aedadb;
}
#content-right{
    margin-top: -3em;
}
.imgbgr {
    margin-right: 1.5em;
     margin-top: -1.5em;
     margin-left: -1.5em;
 
}


.pparts {
    position: relative;
}
#wrap {
    position: relative;     
    margin: 0 0 4em 0;
}
#extrapic {
    position: absolute;
    bottom:  15px;
    right: 15px;
}
</style>
 
 <?php    
   $nadpis = false;
   $mytext  = false; 
    if($artcat  > 0){
      $where .= "And pagesMenu = ".$artcat;
    }
    
    $dizquery = "SELECT  *  FROM tblpages Where  pagesLink = '".$finalpage."'  ORDER BY pagesOrder LIMIT 0,1" ;
    $dizres = $link->query($dizquery);
    if ($dizres && mysqli_num_rows($dizres)>0)
    {
        while ($dizrow =  mysqli_fetch_array($dizres)){
           $diz = $dizrow['pagesOrder'];
           $dizMother = $dizrow['pagesMother'];
          
        }
    }
    $nextquery = "SELECT  *  FROM tblpages Where pagesMother = ".$dizMother." And  pagesOrder > ".$diz." ORDER BY pagesOrder  LIMIT 0,1" ;
    $nextres = $link->query($nextquery);
    if ($nextres && mysqli_num_rows($nextres)>0)
    {
        while ($nextrow =  mysqli_fetch_array($nextres)){
            $nextPage = $nextrow['pagesLink'];
        }
    }    
      $prevquery = "SELECT  *  FROM tblpages Where pagesMother = ".$dizMother." And  pagesOrder < ".$diz." ORDER BY  pagesOrder DESC LIMIT 0,1" ;
    $prevres = $link->query($prevquery);
    if ($prevres && mysqli_num_rows($prevres)>0)
    {
        while ($prevrow =  mysqli_fetch_array($prevres)){
            $prevPage = $prevrow['pagesLink'];
        }
    }  
    
    if(strlen($prevPage)>0) {
         echo "<a class='slick-prev' href='/".$section."/".$note."/".$prevPage.".html' >PREV</a>" ;
    }
    if(strlen($nextPage)>0) {
        echo "<a class='slick-next' href='/".$section."/".$note."/".$nextPage.".html' >NEXT</a>" ;
   }
    ?>

     



    <?php

    echo "<div id='wrap'><div id='firstBox'>";
    $ref2query = "SELECT  * , tblarticles.ID AS NID
    FROM tblarticles
    Where  pagesPublic = 1   ".$where." ORDER BY pagesOrder LIMIT 0,1 ";    
    $ref2res = $link->query($ref2query);
   if ($ref2res && mysqli_num_rows($ref2res)>0)
         {
         while ($artrow =  mysqli_fetch_array($ref2res)){
           //  $primaryContentID = $artrow['NID']; 
           //  $wut2 = 2;
            if ($artrow['pagesHead']==1) {
            //echo "<h3>".$artrow['pagesNadpis']."</h3>";
            }
            include ('./inside/content_engine2.php');    
                
         }
     }
     echo "</div><div id='extrapic'>";
     include ('./adminpages/pictogramySmall.php');  
     echo "</div></div>";

    

    echo "<div id='content-right' class='section'>";

    $ref2query = "SELECT  * , tblarticles.ID AS NID
    FROM tblarticles
    Where  pagesHomepage = 1 
    And pagesPublic = 1   ".$where." ORDER BY pagesOrder ";    
    $ref2res = $link->query($ref2query);
   if ($ref2res && mysqli_num_rows($ref2res)>0)
         {
         while ($artrow =  mysqli_fetch_array($ref2res)){
            echo "<h3>".$artrow['pagesNadpis']."</h3>";
            include ('./inside/content_engine2.php');    
         }
     }
    echo "</div>";

   echo "<div id='content-left' class='section'><div id='accordion'>";
   $rowcount = 0;
   $ref2query = "SELECT  * , tblarticles.ID AS NID FROM tblarticles  Where  pagesHomepage = 0   And pagesPublic = 1   ".$where." ORDER BY pagesOrder LIMIT 1,100000000000";    
   $ref2res = $link->query($ref2query);
   if ($ref2res && mysqli_num_rows($ref2res)>0)
        {
        while ($artrow =  mysqli_fetch_array($ref2res)){
           if($rowcount == 0) { $show = ' show '; }else{  $show = '';  }
           if($rowcount == 0) { $colapsed = '   '; }else{  $colapsed = ' collapsed  ';  }
           echo "<div class='card'><div class='card-header' id='heading".$rowcount."'><h5 class='mb-0'><button id='acbttn".$rowcount."' class='btn btn-link ".$colapsed."' data-toggle='collapse' data-target='#collapse".$rowcount."' aria-expanded='true' aria-controls='collapse".$rowcount."'>";
           echo $artrow['pagesNadpis']."</button></h5></div>";

           echo  "<div id='collapse".$rowcount."' class='collapse ".$show."' aria-labelledby='heading".$rowcount."' data-parent='#accordion'><div class='card-body'>";
           include ('./inside/content_engine2.php');   
           echo "</div></div></div>" ;
           $rowcount++;
        }
    }
    echo "</div></div>";
    
?>